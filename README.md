# TM46: Psywave

## Install

Required php extensions: session, json, pdo, pdo_mysql

```bash
composer install
```

Apply migrations. Admin with `username` "admin" and `password` "123":

```sql
insert into admin (username, password) 
values ('admin', '$2y$10$PGx5TsDLE2dEpa8HUjbRZOke3MHoSGNti0/BzEf6pqzeKf9ak4cbS');
```

Database configuration is stored in `config/db.php`

## Run

```bash
php -S localhost:8000 -t public
```
