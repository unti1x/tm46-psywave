ALTER TABLE `task` 
    ADD COLUMN `email` VARCHAR(100) NOT NULL AFTER `id`,
    ADD COLUMN `name` VARCHAR(100) NOT NULL AFTER `email`,
    ADD COLUMN `changed` TINYINT(1) NULL DEFAULT 0 AFTER `text`,
    CHANGE COLUMN `status` `status` ENUM('new', 'in_progress', 'done') NOT NULL DEFAULT 'new' AFTER `name`;

UPDATE `task` 
    INNER JOIN `user` ON `task`.`user_id` = `user`.`id`
    SET `task`.`email` = `user`.`email`,
        `task`.`name` = `user`.`name`;

ALTER TABLE `task` DROP FOREIGN KEY `user_fk`;

ALTER TABLE `task` 
    DROP COLUMN `user_id`,
    DROP INDEX `user_fk_idx`;

DROP TABLE `user`;
