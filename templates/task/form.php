<?php 
/* @var $view Cheddar\Templating\PhpTemplateEngine */
/* @var $task TM46Psywave\Entity\Task */
include __DIR__ . '/../head.php'; 
?>

<div class="container">
    <form action="" method="post">
        <input type="hidden" name="_csrf_token" value="<?= $csrfToken ?>" />
        <div class="form-group">
            <label>Name</label>
            <input class="form-control"
                   type="text"
                   name="task[name]"
                   value="<?= $view->escape($task->getName()) ?>"
                   required="" />

            <?php if(isset($errors['name'])): ?>
                <p class="text-danger">
                    <?= join('; ', $errors['name']) ?>
                </p>
            <?php endif ?>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input class="form-control"
                   type="email"
                   name="task[email]"
                   value="<?= $view->escape($task->getEmail()) ?>"
                   required="" />

            <?php if(isset($errors['email'])): ?>
                <p class="text-danger">
                    <?= join('; ', $errors['email']) ?>
                </p>
            <?php endif ?>
        </div>
        <div class="form-group">
            <label>Text</label>
            <textarea class="form-control" 
                      name="task[text]"
                      required=""
                ><?= $view->escape($task->getText()) ?></textarea>
            <?php if(isset($errors['text'])): ?>
                <p class="text-danger">
                    <?= join('; ', $errors['text']) ?>
                </p>
            <?php endif ?>
        </div>

        <?php if($task->getId()): ?>
            <div class="form-group">
                <label>Status</label>
                <select name="task[status]" class="form-control">
                    <option value="new" 
                        <?= $task->isNew() ? 'selected=""' : '' ?>
                    >
                        New
                    </option>
                    <option value="in_progress"
                        <?= $task->isInProgress() ? 'selected=""' : '' ?>
                    >
                        In progress
                    </option>
                    <option value="done"
                        <?= $task->isDone() ? 'selected=""' : '' ?>
                    >
                        Done
                    </option>
                </select>
            </div>
            <?php if(isset($errors['status'])): ?>
                <p class="text-danger">
                    <?= join('; ', $errors['status']) ?>
                </p>
            <?php endif ?>
        <?php endif ?>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                <?= $task->getId() ? 'Save' : 'Create' ?>
            </button>
        </div>
    </form>
</div>

<?php include __DIR__ . '/../foot.php'; ?>
