<?php 
/* @var $view Cheddar\Templating\PhpTemplateEngine */
include __DIR__ . '/../head.php'; 
?>

<div class="container">
    <div class="alert alert-success">
        The task #<?= $task->getId() ?> has been created.
        <a href="/">Go to the main page</a> or <a href="/task">create</a> another one.
    </div>
</div>

<?php include __DIR__ . '/../foot.php'; ?>
