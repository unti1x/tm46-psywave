<?php   
/* @var $view Cheddar\Templating\PhpTemplateEngine */
include __DIR__ . '/../head.php'; 
?>

<div class="text-center">
    <?php if(isset($error)): ?>
        <div class="alert alert-danger">
            <?= $error ?>
        </div>
    <?php endif ?>
    <form action="/login" class="auth-form" method="post">
        <input type="hidden" name="_csrf_token" value="<?= $csrfToken ?>" />
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" name="username" placeholder="admin" />
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password" placeholder="******" />
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Log in</button>
        </div>
    </form>
</div>

<?php include __DIR__ . '/../foot.php'; ?>
