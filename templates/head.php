<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" href="/styles/styles.css" />
        <title>Task manager</title>
    </head>
    <body>
        <div class="container mt-3 mb-3">
            <?php if(!$user): ?>
                <a href="/login" class="btn btn-primary">
                    log in
                </a>
            <?php else: ?>
                <a href="/logout" class="btn btn-secondary">
                    log out
                </a>
            <?php endif ?>
            <a href="/task" class="btn btn-outline-secondary">
                new task
            </a>
        </div>
