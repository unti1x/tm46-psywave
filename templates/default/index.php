<?php 
/* @var $view Cheddar\Templating\PhpTemplateEngine */
include __DIR__ . '/../head.php'; 
?>

<div class="container">
    <div class="row mb-3">
        <div class="col-1 d-flex align-items-center">
            Sort
        </div>
        <div class="col">
            <a href="/?page=<?= $page ?>&amp;sort=name&amp;order=ASC"
               class="btn btn-<?= $sort === 'name' && $order === 'ASC' ? 'primary' : 'outline-secondary' ?>"
            >
                &uparrow; name
            </a>
            <a href="/?page=<?= $page ?>&amp;sort=name&amp;order=DESC"
               class="btn btn-<?= $sort === 'name' && $order === 'DESC' ? 'primary' : 'outline-secondary' ?>">
                &downarrow; name
            </a>
            <a href="/?page=<?= $page ?>&amp;sort=email&amp;order=ASC"
               class="btn btn-<?= $sort === 'email' && $order === 'ASC' ? 'primary' : 'outline-secondary' ?>">
                &uparrow; email
            </a>
            <a href="/?page=<?= $page ?>&amp;sort=email&amp;order=DESC"
               class="btn btn-<?= $sort === 'email' && $order === 'DESC' ? 'primary' : 'outline-secondary' ?>">
                &downarrow; email
            </a>
            <a href="/?page=<?= $page ?>&amp;sort=status&amp;order=ASC"
               class="btn btn-<?= $sort === 'status' && $order === 'ASC' ? 'primary' : 'outline-secondary' ?>">
                &uparrow; status
            </a>
            <a href="/?page=<?= $page ?>&amp;sort=status&amp;order=DESC"
               class="btn btn-<?= $sort === 'status' && $order === 'DESC' ? 'primary' : 'outline-secondary' ?>">
                &downarrow; status
            </a>
        </div>
    </div>
    <?php 
    if(count($tasks)):
        /* @var $task Task */
        foreach($tasks as $task):
        ?>
            <div class="task">
                <div class="task-status">
                    <a href="/task/<?= $task->getId() ?>">
                        #<?= $task->getId() ?>
                    </a>
                    
                    [<?= $task->getStatusText() ?>]
                </div>
                <?php if($task->isChanged()): ?>
                    <div class="text-muted">Modified by admin</div>
                <?php endif ?>
                <div class="task-user">
                    <?= $view->escape($task->getName()) ?>
                    &lt;<?= $view->escape($task->getEmail()) ?>&gt;
                </div>
                <div class="task-content">
                    <?= $view->escape($task->getText()) ?>
                </div>
            </div>
        <?php
        endforeach;
        
        if($maxPages > 1):
        ?>
            <nav>
              <ul class="pagination">
                <?php for($i = 1; $i <= $maxPages; $i++): ?>
                    <li class="page-item <?= $i === $page ? 'active' : '' ?>">
                        <a class="page-link" 
                           href="/?page=<?= $i ?>&amp;sort=<?= $sort ?>&amp;order=<?= $order ?>"
                        >
                            <?= $i ?>
                        </a>
                    </li>
                <?php endfor; ?>
              </ul>
            </nav>
        <?php
        endif;
    endif;
    ?>
</div>

<?php include __DIR__ . '/../foot.php'; ?>
