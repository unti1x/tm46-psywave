<?php

use Cheddar\Security\User\{UserProviderInterface, DatabaseProvider};
use Cheddar\Security\Authentication\{AuthenticatorInterface, BasicAuthenticator};

return [
    'services' => [
        '@templating' => Cheddar\Templating\PhpTemplateEngine::class,
        '@authenticator' => \Cheddar\Security\Authentication\BasicAuthenticator::class,
        AuthenticatorInterface::class => BasicAuthenticator::class,
        UserProviderInterface::class => DatabaseProvider::class
    ]
];
