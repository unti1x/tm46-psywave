<?php

use Cheddar\Templating\PhpTemplateEngine;

return [
    PhpTemplateEngine::class => [
        'templatesDir' => __DIR__ . '/../templates'
    ]
];
