<?php

use Cheddar\DBAL\Schema;

use TM46Psywave\Repository\{AdminRepository, TaskRepository};
use TM46Psywave\Entity\{Task, User, Admin};

return [
    Schema::class => [
        Task::class => [
            'table' => 'task',
            'repository' => TaskRepository::class,
            'fields' => [
                'id' => ['type' => 'int'],
                'name' => ['type' => 'string'],
                'email' => ['type' => 'string'],
                'text' => ['type' => 'string'],
                'status' => ['type' => 'string'],
                'changed' => ['type' => 'bool']
            ]
        ],
        Admin::class => [
            'table' => 'admin',
            'repository' => AdminRepository::class,
            'fields' => [
                'id' => ['type' => 'int'],
                'username' => ['type' => 'string'],
                'password' => ['type' => 'string']
            ]
        ]
    ]
];
