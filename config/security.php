<?php

use Cheddar\Security\Authentication\BasicAuthenticator;
use Cheddar\Security\User\DatabaseProvider;
use Cheddar\Security\CsrfGenerator;
use TM46Psywave\Entity\Admin;

return [
    BasicAuthenticator::class => [
        'usernameField' => 'username'
    ],
    DatabaseProvider::class => [
        'userClass' => Admin::class
    ],
    CsrfGenerator::class => [
        'secret' => 'notsosecretchangeme'
    ]
];
