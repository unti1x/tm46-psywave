<?php

use TM46Psywave\Controller;
use Cheddar\Router\RouteMatcher;

return [
    RouteMatcher::class => [
        [
            'route' => '/^\/$/',
            'action' => [Controller\DefaultController::class, 'index']
        ],
        [
            'route' => '/^\/login$/',
            'methods' => ['GET'],
            'action' => [Controller\AuthController::class, 'index']
        ],
        [
            'route' => '/^\/login$/',
            'methods' => ['POST'],
            'action' => [Controller\AuthController::class, 'auth']
        ],
        [
            'route' => '/^\/logout$/',
            'action' => [Controller\AuthController::class, 'logout']
        ],
        [
            'route' => '/^\/task$/',
            'action'=> [Controller\TaskController::class, 'new'],
            'methods' => ['GET']
        ],
        [
            'route' => '/^\/task$/',
            'action'=> [Controller\TaskController::class, 'create'],
            'methods' => ['POST']
        ],
        [
            'route' => '/^\/task\/(?P<id>\d+)$/',
            'action'=> [Controller\TaskController::class, 'edit'],
            'methods' => ['GET'],
            'auth' => true
        ],
        [
            'route' => '/^\/task\/(?P<id>\d+)$/',
            'action'=> [Controller\TaskController::class, 'update'],
            'methods' => ['POST'],
            'auth' => true
        ],
    ]
];
