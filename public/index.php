<?php
use TM46Psywave\Kernel;
use Cheddar\Http\Request;

include_once '../vendor/autoload.php';

$kernel = new Kernel();
$kernel->boot();

$request = new Request($_GET, $_POST, $_SERVER);
$response = $kernel->handleRequest($request);

$kernel->send($response);
