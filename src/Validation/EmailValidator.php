<?php

namespace TM46Psywave\Validation;

/**
 * @author unti1x
 */
class EmailValidator implements FieldValidatorInterface
{
    
    private const EMAIL_PATTERN = '/^[\da-z\_\.\-]*[\da-z]'
                                . '@[\da-z\_\.\-]*[\da-z]'
                                . '\.[a-z]{2,8}$/i';

    public function getMessage(string $field, $value): string
    {
        return "Field '$field' should be a proper email address";
    }

    public function isValid($value): bool
    {
        $matched = preg_match(self::EMAIL_PATTERN, $value); 
        return $matched === 1 && strlen($value) < 256;
    }

    
}
