<?php

namespace TM46Psywave\Validation;

/**
 * @author unti1x
 */
abstract class AbstractValidator
{
    
    abstract protected function rules(): array;
    
    protected function validateField(FieldValidatorInterface $validator, string $field, $value): ?string
    {
        $message = null;
        
        if(!$validator->isValid($value)) {
            $message = $validator->getMessage($field, $value);
        }
        
        return $message;
    }
    
    public function validate($data, array $rules = null): ?array
    {
        $errors = [];
        
        if(!$rules) {
            $rules = $this->rules();
        }
        
        foreach($rules as $field => $validators) {
            
            foreach ($validators as $validator) {
                $message = $this->validateField($validator, $field, $data[$field]);
                if($message) {
                    $errors[$field][] = $message;
                }
            }
            
        }
        
        return $errors;
    }
    
}
