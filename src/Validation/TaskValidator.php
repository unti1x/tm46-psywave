<?php

namespace TM46Psywave\Validation;

use TM46Psywave\Entity\Task;

/**
 * @author unti1x
 */
class TaskValidator extends AbstractValidator
{
    
    protected function rules(): array
    {
        $required = new RequiredValidator();
        return [
            'email' => [$required, new EmailValidator()],
            'name' => [$required],
            'text' => [$required],
            'status' => [new ValuesValidator(Task::STATUSES, true)]
        ];
    }
    
}
