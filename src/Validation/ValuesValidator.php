<?php

namespace TM46Psywave\Validation;

/**
 * @author unti1x
 */
class ValuesValidator implements FieldValidatorInterface
{
    
    private array $values;
    
    private bool $allowEmpty;


    public function __construct(array $values, bool $allowEmpty = true)
    {
        $this->values = $values;
        $this->allowEmpty = $allowEmpty;
    }
    
    public function getMessage(string $field, $value): string
    {
        $values = join(', ', $this->values);
        return "Field '$field' must be one of [$values], got '$value'";
    }

    public function isValid($value): bool
    {
        return ($this->allowEmpty && empty($value)) || in_array($value, $this->values);
    }
    
}
