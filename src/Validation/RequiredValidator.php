<?php

namespace TM46Psywave\Validation;

/**
 *
 * @author unti1x
 */
class RequiredValidator implements FieldValidatorInterface
{
    
    public function getMessage(string $field, $value): string
    {
        return "Field '$field' is required";
    }

    public function isValid($value): bool
    {
        return !empty($value) && !(is_string($value) && empty(trim($value)));
    }
    
}
