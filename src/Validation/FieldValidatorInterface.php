<?php

namespace TM46Psywave\Validation;

/**
 * @author unti1x
 */
interface FieldValidatorInterface
{
    
    public function isValid($value): bool;
    
    public function getMessage(string $field, $value): string;
    
}
