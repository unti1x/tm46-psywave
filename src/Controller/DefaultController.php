<?php

namespace TM46Psywave\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Http\{Response, Request};
use TM46Psywave\Repository\TaskRepository;

class DefaultController extends AbstractController
{
    
    public function index(TaskRepository $repo, Request $request): Response
    {
        $sort = $request->getQuery()->get('sort') ?: 'status';
        $order = $request->getQuery()->get('order') ?: 'ASC';
        $page = (int)$request->getQuery()->get('page') ?: 1;
        
        $tasks = $repo->findPage($sort, $order, $page);
        $maxPages = $repo->countPages();
        
        return $this->render('default/index.php', [
            'tasks' => $tasks,
            'page' => $page,
            'maxPages' => $maxPages,
            'sort' => $sort,
            'order' => $order
        ]);
    }
    
}
