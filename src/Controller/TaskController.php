<?php

namespace TM46Psywave\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Http\Exception\NotFoundException;
use Cheddar\Security\CsrfGenerator;
use Cheddar\Http\{Request, ResponseInterface};
use TM46Psywave\Entity\Task;
use TM46Psywave\Repository\TaskRepository;
use TM46Psywave\Validation\TaskValidator;

/**
 *
 * @author unti1x
 */
class TaskController extends AbstractController
{
    
    public function new(CsrfGenerator $csrf)
    {
        $task = new Task();

        return $this->render('task/form.php', [
            'task' => $task,
            'errors' => [],
            'csrfToken' => $csrf->generateToken()
        ]);
    }
    
    private function updateTask(Task $task, array $data): void
    {
        $text = trim($data['text'] ?? '');
        
        if($text !== $task->getText()) {
            $task->setChanged(true);
        }
        
        $task->setText($text)
            ->setEmail($data['email'] ?? '')
            ->setName($data['name'] ?? '');

        $status = $data['status'] ?? null;
        
        if($status) {
            $task->setStatus($status);
        }
    }
    
    public function create(
        Request $request,
        TaskRepository $taskRepo,
        CsrfGenerator $csrf,
        TaskValidator $validator
    ): ResponseInterface
    {
        $token = $request->getRequest()->get('_csrf_token');
        if(!$csrf->validate($token)) {
            throw new \RuntimeException('csrf token expired');
        }
        
        $task = new Task();
        $data = $request->getRequest()->get('task');
        
        $this->updateTask($task, $data);
        $errors = $validator->validate($data);

        if(empty($errors)) {
            $taskRepo->insert($task);
            return $this->render('task/success.php', [
                'task' => $task
            ]);
        }
        
        return $this->render('task/form.php', [
            'task' => $task,
            'errors' => $errors,
            'csrfToken' => $csrf->generateToken()
        ]);
    }
    
    public function edit(int $id, TaskRepository $taskRepo, CsrfGenerator $csrf)
    {
        $task = $taskRepo->find($id);
        if(!$task) {
            throw new NotFoundException('Task not found');
        }
        
        return $this->render('task/form.php', [
            'task' => $task,
            'errors' => [],
            'csrfToken' => $csrf->generateToken()
        ]);
    }
    
    public function update(
        int $id,
        Request $request,
        TaskRepository $taskRepo,
        CsrfGenerator $csrf,
        TaskValidator $validator
    )
    {
        $token = $request->getRequest()->get('_csrf_token');
        if(!$csrf->validate($token)) {
            throw new \RuntimeException('csrf token expired');
        }
        
        $task = $taskRepo->find($id);
        if(!$task) {
            throw new NotFoundException('Task not found');
        }
        
        $data = $request->getRequest()->get('task');
        $this->updateTask($task, $data);
        $errors = $validator->validate($data);

        if(empty($errors)) {
            $taskRepo->update($task);
            return $this->redirect('/');
        }
        
        return $this->render('task/form.php', [
            'task' => $task,
            'errors' => $errors,
            'csrfToken' => $csrf->generateToken()
        ]);
    }
}
