<?php

namespace TM46Psywave\Controller;

use Cheddar\Controller\AbstractController;
use Cheddar\Security\Authentication\AuthenticatorInterface;
use Cheddar\Security\User\UserProviderInterface;
use Cheddar\Security\{Guard, CsrfGenerator};
use Cheddar\Security\Exception\UserNotFoundException;
use Cheddar\Http\{Request, ResponseInterface, RedirectResponse};

/**
 * Description of AuthController
 *
 * @author unti1x
 */
class AuthController extends AbstractController
{
    
    public function index(CsrfGenerator $csrf): ResponseInterface
    {
        return $this->render('auth/index.php', [
            'csrfToken' => $csrf->generateToken()
        ]);
    }
    
    public function auth(
        Request $request,
        AuthenticatorInterface $authenticator,
        UserProviderInterface $userProvider,
        Guard $guard,
        CsrfGenerator $csrf
    ): ResponseInterface
    {
        $error = '';
        
        $credentials = $authenticator->getCredentials($request);
        $token = $request->getRequest()->get('_csrf_token');
        if(!$csrf->validate($token)) {
            throw new \RuntimeException('csrf token expired');
        }
        
        if($credentials['username'] && $credentials['password']) {

            try {
                $user = $userProvider->loadByUsername($credentials['username']);

                if($authenticator->checkCredentials($credentials, $user)) {
                    $guard->setSessionUsername($credentials['username']);
                    return $authenticator->onSuccess($request);
                }

                $error = 'Wrong password';

            } catch (UserNotFoundException $ex) {
                $error = $ex->getMessage();
            }
            
        } else {
            $error = 'Enter username and password';
        }
        
        return $this->render('auth/index.php', [
            'error' => $error,
            'csrfToken' => $csrf->generateToken()
        ]);
    }
    
    
    public function logout(Guard $guard): RedirectResponse
    {
        $guard->logout();
        return $this->redirect('/');
    }
}
