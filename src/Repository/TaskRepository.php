<?php

namespace TM46Psywave\Repository;

use Cheddar\DBAL\{AbstractRepository, EntityManager, DatabaseAdapter, Query};
use TM46Psywave\Entity\Task;

/**
 *
 * @author unti1x
 */
class TaskRepository extends AbstractRepository
{
    const PAGE_SIZE = 3;
    
    public function __construct(EntityManager $em, DatabaseAdapter $adapter)
    {
        parent::__construct($em, $adapter);
        $this->setEntity(Task::class);
    }
    
    public function countPages(): int
    {
        $query = 'SELECT COUNT(*) AS qty FROM `task`';
        [$result] = $this->executeSql($query);
        return ceil((int)$result['qty'] / self::PAGE_SIZE);
    }
    
    public function findPage(
        string $sort = 'status',
        string $order = Query::ORDER_ASC, 
        int $page = 1
    ): array
    {
        $query = $this->getQuery()
            ->limit(self::PAGE_SIZE, ($page - 1) * self::PAGE_SIZE);

        if(in_array($sort, ['status', 'email', 'name'])) {
            $query->sortBy($sort, $order);
        }
        
        return $this->getResult($query);
    }

    public function insert(Task $task)
    {
        $query = 'INSERT INTO `task` (`text`, `email`, `name`) VALUES (:text, :email, :name)';
        $id = $this->executeSql($query, [
            ':text' => $task->getText(),
            ':email' => $task->getEmail(),
            ':name' => $task->getName()
        ]);
        
        $task->setId($id);
        
        return $id;
    }
    
    public function update(Task $task)
    {
        $query = 'UPDATE `task` SET '
            . '`text` = :text, '
            . '`status` = :status, '
            . '`email` = :email, '
            . '`name` = :name, '
            . '`changed` = :changed '
            . 'WHERE id = :id';
        
        $this->executeSql($query, [
            ':id' => $task->getId(),
            ':text' => $task->getText(),
            ':email' => $task->getEmail(),
            ':name' => $task->getName(),
            ':status' => $task->getStatus(),
            ':changed' => (int)$task->isChanged()
        ]);
    }
    
}
