<?php
namespace TM46Psywave\Repository;

use Cheddar\Security\User\{UserRepositoryInterface, UserInterface};
use Cheddar\DBAL\AbstractRepository;
use Cheddar\DBAL\EntityManager;
use Cheddar\DBAL\DatabaseAdapter;
use TM46Psywave\Entity\Admin;

/**
 *
 * @author unti1x
 */
class AdminRepository extends AbstractRepository implements UserRepositoryInterface
{
    
    public function __construct(EntityManager $em, DatabaseAdapter $adapter)
    {
        parent::__construct($em, $adapter);
        $this->setEntity(Admin::class);
    }
    
    public function findByUsername(string $username): ?UserInterface
    {
        $query = $this->getQuery()->filter('username = :username')
            ->setParameters([
                ':username' => $username
            ]);
        
        return $this->getSingleResult($query);
    }

}
