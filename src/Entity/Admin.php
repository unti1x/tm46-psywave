<?php
namespace TM46Psywave\Entity;

use Cheddar\Security\User\UserInterface;

/**
 * @author unti1x
 */
class Admin implements UserInterface
{
    
    private int $id;
    
    private string $username;
    
    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @see UserInterface
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
    
    public function setUsername(string $username)
    {
        $this->username = $username;
        return $this;
    }
    
    /**
     * @see UserInterface
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }
    
}
