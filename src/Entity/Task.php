<?php

namespace TM46Psywave\Entity;

/**
 * @author unti1x
 */
class Task
{
    
    public const STATUS_NEW = 'new';

    public const STATUS_IN_PROGRESS = 'in_progress';

    public const STATUS_DONE = 'done';
    
    public const STATUSES = [
        self::STATUS_NEW, self::STATUS_IN_PROGRESS, self::STATUS_DONE
    ];
    
    private ?int $id = null;
    
    private string $name = '';
    
    private string $email = '';
    
    private string $text = '';
    
    private string $status = self::STATUS_NEW;

    private bool $changed = false;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function setText(string $text)
    {
        $this->text = trim($text);
        return $this;
    }

    public function setStatus(string $status)
    {
        if(!in_array($status, self::STATUSES)) {
            throw new \UnexpectedValueException();
        }
        $this->status = $status;
        return $this;
    }
    
    public function getStatusText(): string
    {
        $statusText = 'Unknown';
        
        switch ($this->status) {
            case self::STATUS_NEW:
                $statusText = 'New';
                break;
            
            case self::STATUS_IN_PROGRESS:
                $statusText = 'In progress';
                break;
            
            case self::STATUS_DONE:
                $statusText = 'Done';
                break;
        }
        
        return $statusText;
    }
    
    public function isNew(): bool
    {
        return $this->status === self::STATUS_NEW;
    }
    
    public function isInProgress(): bool
    {
        return $this->status === self::STATUS_IN_PROGRESS;
    }
    
    public function isDone(): bool
    {
        return $this->status === self::STATUS_DONE;
    }
    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setName(string $name)
    {
        $this->name = trim($name);
        return $this;
    }

    public function setEmail(string $email)
    {
        $this->email = trim($email);
        return $this;
    }

    public function setChanged(bool $changed): self
    {
        $this->changed = $changed;
        return $this;
    }
    
    public function isChanged(): bool
    {
        return $this->changed;
    }
    
}
